; Drupal core
core = 7.57
api = 2

; Panopoly and overrides.
; These go first so they get overridden on conflicts.

projects[panopoly_admin][subdir] = panopoly
projects[panopoly_admin][version] = 1.13

; @todo: Remove when we update Panopoly Admin.
projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][subdir] = contrib

projects[panopoly_core][subdir] = panopoly
projects[panopoly_core][version] = 1.13
projects[panopoly_core][patch][2840631] = https://www.drupal.org/files/issues/panopoly_core-panelizer_term_plugin-2840631-2.patch

projects[date][subdir] = contrib
projects[date][version] = 2.10

; @todo: Remove when we update Panopoly Core.
projects[fieldable_panels_panes][version] = 1.7
projects[fieldable_panels_panes][subdir] = contrib
projects[fieldable_panels_panes][patch][2562717] = https://www.drupal.org/files/issues/fpp-empty-admin-title-2562717-2.patch

projects[jquery_update][version] = 3.0-alpha5
projects[jquery_update][subdir] = contrib

projects[panelizer][version] = 3.4
projects[panelizer][subdir] = contrib

projects[token][version] = 1.7
projects[token][subdir] = contrib

projects[panopoly_images][subdir] = panopoly
projects[panopoly_images][version] = 1.13
projects[panopoly_images][patch][imgareaselectlocation] = "patches/panopoly_images-update_imgareaselect_loc.patch"

projects[panopoly_pages][subdir] = panopoly
projects[panopoly_pages][version] = 1.13

projects[panopoly_search][subdir] = panopoly
projects[panopoly_search][version] = 1.13
; Update location of SolrPhpClient to github.
projects[panopoly_search][patch][2823668] = https://www.drupal.org/files/issues/solrphpclient_download-2823668-3.patch

projects[panopoly_theme][subdir] = panopoly
projects[panopoly_theme][version] = 1.13

projects[panopoly_widgets][subdir] = panopoly
projects[panopoly_widgets][version] = 1.13
projects[panopoly_widgets][patch][2864669] = https://www.drupal.org/files/issues/prevent-errors-if-no-jquery-tabs.patch

projects[panopoly_wysiwyg][subdir] = panopoly
projects[panopoly_wysiwyg][version] = 1.13

projects[pathauto][version] = 1.3
projects[pathauto][subdir] = contrib

; Override file_entity and media requirements in panopoly_widgets.
projects[file_entity][version] = 2.20
projects[file_entity][subdir] = contrib

projects[media][subdir] = 2.19
projects[media][subdir] = contrib

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.3

projects[linkit][subdir] = contrib
projects[linkit][download][url] = http://git.drupal.org/project/linkit.git
projects[linkit][download][revision] = 1500537e3f3992552fab1434fb1eecadcd9fd266
projects[linkit][download][branch] = 7.x-3.x

; Utilities

projects[collapsiblock][subdir] = contrib
projects[collapsiblock][download][url] = http://git.drupal.org/project/collapsiblock.git
projects[collapsiblock][download][revision] = 5ad9824a2a77c0fcee6f49bf1d7f5afbbee25e8b
projects[collapsiblock][download][branch] = 7.x-1.x
; Issue #1972720: Default values must be lower case.
projects[collapsiblock][patch][1972720] = http://www.drupal.org/files/collapsiblock-default_value_mixed_case_delta-1972720-2.patch
; Get complete theme settings.
projects[collapsiblock][patch][2332401] = http://www.drupal.org/files/issues/2332401-4-get_theme_settings_properly.patch

projects[email_registration][subdir] = contrib
projects[email_registration][version] = 1.3

projects[field_group][version] = 1.5
projects[field_group][subdir] = contrib
projects[field_group][patch][2715097] = http://www.drupal.org/files/issues/2715097-2.patch

projects[flood_unblock][subdir] = contrib
projects[flood_unblock][version] = 1.4

projects[memcache][subdir] = contrib
projects[memcache][version] = 1.3

projects[mollom][subdir] = contrib
projects[mollom][version] = 2.15

projects[password_policy][subdir] = contrib
projects[password_policy][version] = 1.15

projects[views_data_export][subdir] = contrib
projects[views_data_export][version] = 3.2

; Editing

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.20.1/ckeditor_4.20.1_full.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"

projects[wysiwyg_filter][version] = 1.6-rc2
projects[wysiwyg_filter][subdir] = contrib
projects[wysiwyg_filter][patch][1931306] = https://www.drupal.org/files/wysiwyg_filter-style-groups-alter-1931306-1.patch
projects[wysiwyg_filter][patch][2319949] = https://www.drupal.org/files/issues/wysiwyg_filter-2319949-6.patch

; Themes
projects[shiny][type] = theme
projects[shiny][version] = 1.6

projects[omega][type] = theme
projects[omega][download][url] = http://git.drupal.org/project/omega.git
projects[omega][download][revision] = 80d24bbc0662de64feae31a5ab55e9482ece7036
projects[omega][download][branch] = 7.x-3.x

projects[omega_kickstart][type] = theme
projects[omega_kickstart][version] = 3.4

; Open CRM

projects[opencrm][subdir] = contrib
projects[opencrm][download][url] = http://git.drupal.org/project/opencrm.git
projects[opencrm][download][revision] = 8eaa5a063cceefe50dc885cc31831dbec5d5966c
projects[opencrm][download][branch] = 7.x-1.x
