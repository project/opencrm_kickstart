#!/bin/bash

# Fail on first error
set -e

# Sort out our options
ROOT=true # r - skip cleanup
WORKING="" # w - check out working copies when making
QUIET="-q" # v - enable verbose output

DRUSH=true # d - run without drush (maintenance mode and updates)
UPDATE=true # u - skip database updates (irrelevant if without drush)

CORE=false # c - rebuild Drupal core
PROFILE=false # p - rebuild the OpenCRM Kickstart profile
SITE=false # s - rebuild any site make files according to .makefiles in site dir

while getopts ":durcpswv" opt; do
  case $opt in
    d)
      DRUSH=false
      ;;
    r)
      ROOT=false
      ;;
    c)
      CORE=true
      ;;
    p)
      PROFILE=true
      ;;
    s)
      SITE=true
      ;;
    u)
      UPDATE=false
      ;;
    w)
      WORKING="--working-copy --no-gitinfofile"
      ;;
    v)
      QUIET=""
      ;;
    \?)
      # Unknown option...
      echo "Invalid option: -$OPTARG"
      exit
      ;;
  esac
done

# Get rid of the options from our arguments.
numargs=$#
for ((i = 1; i <= $numargs; i++))
do
  case $1 in
    -*)
      shift
      ;;
  esac
done

# If we're updating root, check we have a valid tag.
if [ -n "$1" ]
then
  echo "Fetching and checking tag/commit."
  git fetch

  # Check the commit/tag exists
  if git show-ref --tags --quiet --verify -- "refs/tags/$1"
  then :;
  elif git rev-list $1>/dev/null 2>&1
  then :;
  else
    echo "\"$1\" is not a valid tag or commit."
    exit
  fi
fi

# Go into maintenance mode
if $DRUSH
then
  echo "Entering maintentance mode."
  eval "drush vset $QUIET --always-set maintenance_mode 1"
fi

# Clean up core if we're rebuilding it
if $CORE
then
  # Make sure we have write permissions in sites/default
  if [ ! -w "sites/default" ]
  then
    chmod 0755 sites/default
  fi

  if $ROOT
  then
    echo "Preparing core for rebuild."

    eval "`dirname $0`/clean.sh ff"
  fi
fi

# Clean up the profile if we're rebuilding it
if $PROFILE && $ROOT && [ -d "profiles/opencrm_kickstart" ]
then
  echo "Preparing profile for rebuild."

  cd profiles/opencrm_kickstart
  git clean -ffxd
  cd ../..
fi

# If requested, check out and update sub-modules
if [ -n "$1" ]
then
  echo "Updating repository."

  git checkout $1
  git submodule sync
  git submodule update --init
fi

# Run the core make script.
if $CORE
then
  echo "Building core."

  eval "drush make $QUIET profiles/opencrm_kickstart/drupal-org-core.make . $WORKING"
fi

# Run the profile make script.
if $PROFILE
then
  echo "Building profile."

  eval "drush make $QUIET --no-core --contrib-destination=\"profiles/opencrm_kickstart\" profiles/opencrm_kickstart/drupal-org.make . $WORKING"
fi

# Run any make files we need to in the sites folder.
file="sites/default/.build_make"
if $SITE && [ -f "sites/default/.build_make" ]
then
  echo "Building site."

  while read -r line; do
    [[ "$line" = "" ]] && continue
    [[ "$line" = \#* ]] && continue
    eval "drush make $QUIET --no-core --contrib-destination=\"sites/all\" $line . $WORKING"
  done < "sites/default/.build_make"
fi

# Run any drush scripts that are required.
if $DRUSH
then
  # Clear the registry in case things have changed.
  echo "Rebuilding registry."
  eval "drush rr $QUIET --no-cache-clear"

  # Run any site specific post-build code.
  if [ -f "sites/default/.build_post" ]
  then
    echo "Running site post build."
    . sites/default/.build_post
  fi

  # Run updates if required.
  if $UPDATE
  then
    echo "Running updates."
    eval "drush updb -y"
  fi

  # Leave maintenance mode
  echo "Exiting maintenance mode."
  eval "drush vset $QUIET --always-set maintenance_mode 0"
  eval "drush cc all $QUIET"
fi

echo "Rebuild complete."
