# Get our clean ignores.
command="git clean -$1xd -e sites/default "

if [ -f ".cleanignore" ]
then
  while read -r line; do
    [[ "$line" = "" ]] && continue
    [[ "$line" = \#* ]] && continue
    command="$command -e $line"
  done < ".cleanignore"
fi

# Execute the command.
eval $command
