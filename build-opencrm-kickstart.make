api = 2
core = 7.x

; Drupal
projects[drupal][type] = core
projects[drupal][version] = 7.34

; opencrm_kickstart
projects[opencrm_kickstart][type] = profile
projects[opencrm_kickstart][download][type] = git
projects[opencrm_kickstart][download][url] = http://git.drupal.org/project/opencrm_kickstart.git
projects[opencrm_kickstart][download][branch] = 7.x-1.x
