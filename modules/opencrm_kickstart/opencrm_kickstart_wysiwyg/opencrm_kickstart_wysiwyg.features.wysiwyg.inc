<?php
/**
 * @file
 * panopoly_wysiwyg.features.wysiwyg.inc
 */
 
/**
 * Implements hook_wysiwyg_default_profiles_alter().
 */
function opencrm_kickstart_wysiwyg_wysiwyg_default_profiles_alter(&$profiles) {
  // Updated Panopoly profiles to use CKEditor
  $profiles['panopoly_wysiwyg_text']['editor'] = 'ckeditor';
  $profiles['panopoly_wysiwyg_text']['settings']['buttons'] = array(
    'default' => array(
      'Bold' => 1,
      'Italic' => 1,
      'Underline' => 1,
      'JustifyLeft' => 1,
      'JustifyCenter' => 1,
      'JustifyRight' => 1,
      'JustifyBlock' => 1,
      'BulletedList' => 1,
      'NumberedList' => 1,
      'Link' => 1,
      'Blockquote' => 1,
      'Scayt' => 1,
    ),
    'linkit' => array(
      'linkit' => 1,
    ),
    'drupal' => array(
      'media' => 1,
    ),
  );
}

