<?php
/**
 * @file
 * opencrm_kickstart_wysiwyg.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function opencrm_kickstart_wysiwyg_filter_default_formats() {
  $formats = array();

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'media_filter' => array(
        'weight' => '2',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
