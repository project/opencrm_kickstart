<?php
/**
 * @file
 * opencrm_kickstart_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages_alter().
 */
function opencrm_kickstart_core_default_page_manager_pages_alter(&$pages) {
  // Set the right layout on dashboard pages that we know about.
  $toSet = array(
    'party_dashboard_view' => 'page_party_dashboard_view__panel_context',
    'party_dashboard' => 'page_party_dashboard_panel_context',
  );

  foreach ($toSet as $name => $handler) {
    if (!empty($pages[$name]->default_handlers[$handler])) {
      $display = &$pages[$name]->default_handlers[$handler]->conf['display'];

      // Only swap if we were using defaults.
      if ($display->layout != 'dashboard') {
        continue;
      }

      $display->layout = 'sidebar';
      $display->layout_settings = array(
        'items' => array(
          'canvas' => array(
            'type' => 'row',
            'contains' => 'column',
            'children' => array(
              0 => 'main',
            ),
            'parent' => NULL,
          ),
          'main' => array(
            'type' => 'column',
            'width' => 100,
            'width_type' => '%',
            'children' => array(
              0 => 'main-row',
            ),
            'parent' => 'canvas',
          ),
          'main-row' => array(
            'type' => 'row',
            'contains' => 'region',
            'children' => array(
              0 => 'sidebar',
              1 => 'content',
            ),
            'parent' => 'main',
          ),
          'content' => array(
            'type' => 'region',
            'title' => 'Content',
            'width' => '100',
            'width_type' => '%',
            'parent' => 'main-row',
            'class' => '',
          ),
          'sidebar' => array(
            'type' => 'region',
            'title' => 'Sidebar',
            'width' => '300',
            'width_type' => 'px',
            'parent' => 'main-row',
            'class' => '',
          ),
        ),
      );

      // Turn sidebar to left and content to main
      $display->panels['sidebar'] = $display->panels['left'];
      $display->panels['content'] = $display->panels['main'];

      // Rearange all panes.
      foreach ($display->content as &$pane) {
        switch ($pane->panel) {
          case 'left':
            $pane->panel = 'sidebar';
            break;
          case 'main':
            $pane->panel = 'content';
            break;
        }
      }
    }
  }
}
