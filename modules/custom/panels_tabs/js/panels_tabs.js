(function ($) {

/**
 * JS related to the tabs in the Panels tabs.
 */
Drupal.behaviors.panelsTabs = {
  attach: function (context) {
    $.each(Drupal.settings.panelsTabs.tabsID, function(selector, options) {
      var tabs = $('#' + selector +':not(.tabs-processed)', context);

      if (options.cookie) {
        var active = $('a[href="' + $.cookie(options.cookie) + '"]', tabs) ;
        if (active.length) {
          options.active = active.parent().index();
        }
        options.activate = function(event, ui) {
          $.cookie(options.cookie, ui.newTab.find('a').attr('href'));
        }
      }

      tabs.addClass('tabs-processed')
        .tabs(options)
        .find('.processed-hide').hide();
    });
  },
  detach: function (context) {
    // Make sure we don't loose the cookie when the tabs are destroyed (e.g.
    // ajax insert commands). We'll store the current tab back to the options.
    $.each(Drupal.settings.panelsTabs.tabsID, function(selector, options) {
      Drupal.settings.panelsTabs.tabsID[selector].selected = $('#' + selector).tabs('option', 'selected');
    });
  }
};

})(jQuery);


