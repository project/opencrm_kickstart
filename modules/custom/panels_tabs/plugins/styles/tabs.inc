<?php


/**
 * @file
 * Definition of the 'Tabs' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Tabs'),
  'description' => t('Show panel panes in a region as tabs.'),
  'render region' => 'panels_tabs_style_render_region',
  'settings form' => 'panels_tabs_style_settings_form',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_tabs_style_render_region($vars) {
  // Don't display empty tabs...
  if (count($vars['panes']) == 0) {
    return '';
  }

  // Pull through our variables.
  $display = $vars['display'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];

  // Start our setup.
  $options = array();
  $tab_id = 'tabs-' . $display->css_id;

  // Our main holding element.
  $element = array();

  // Set up our cookie if we're supposed to be using it.
  if (!empty($settings['cookie'])) {
    global $base_path;
    drupal_add_library('system', 'jquery.cookie');
    $options['cookie'] = $tab_id;
  }

  // Get the pane titles.
  $items = array();
  $delta = 1;
  foreach ($panes as $pane_id => $content) {
    // Make sure the pane exists
    if (!empty($panes[$pane_id])) {
      // Get hold of our title and build our anchor.
      $title = panels_tabs_pane_titles($pane_id);
      $title = $title ? $title : t('Tab @delta', array('@delta' => $delta));
      $anchor = drupal_html_id($title, array('lower case' => TRUE));

      // Add our tab navigation item.
      $items[] = array(
        'data' => '<a href="#' . $anchor . '">' . $title . '</a>',
        'class' => array('ui-state-default'),
      );

      // Add our tab pane.
      $pattern = '~([^>]class\s*=\s*["\'].*)(pane-title)(.*[\'"].*>[\s]*' . preg_quote($title) . '[\s]*<)~';
      $replacement = '$1$2 processed-hide$3';
      $content = preg_replace($pattern, $replacement, $content, -1, $count);
      $element['tabs_content'][$pane_id] = array(
        '#prefix' => '<div id="' . $anchor . '" class="clearfix ui-tabs-panel">',
        '#suffix' => '</div>',
        '#markup' => $content,
      );

      ++$delta;
    }
  }

  // Allow for a single tab to not have navigation.
  if (empty($settings['hide_single_tab']) || count($items) > 1) {
    $element += array(
      '#prefix' => '<div id="' . $tab_id . '" class="ui-tabs ui-widget ui-widget-content">',
      '#suffix' => '</div>',
    );
    $element['#attached']['library'][] = array('system', 'ui.tabs');
    $element['#attached']['js'][drupal_get_path('module', 'panels_tabs') . '/js/panels_tabs.js'] = array('type' => 'file');
    // Add our settings so the behavior creates the tabs.
    $element['#attached']['js'][] = array(
      'type' => 'setting',
      'data' => array(
        'panelsTabs' => array(
          'tabsID' => array(
            $tab_id => $options,
          ),
        ),
      ),
    );

    $element['tabs_title'] = array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => array(
        'class' => array('ui-tabs-nav', 'ui-helper-clearfix'),
      ),
      '#weight' => -99,
    );
  }

  return drupal_render($element);
}

/**
 * Settings form callback.
 */
function panels_tabs_style_settings_form($style_settings) {
  $form['cookie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remember tab with cookie'),
    '#default_value' => (isset($style_settings['cookie'])) ? $style_settings['cookie'] : 0,
  );

  $form['hide_single_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide tabs if only one tab visible'),
    '#default_value' => (isset($style_settings['hide_single_tab'])) ? $style_settings['hide_single_tab'] : 0,
  );

  return $form;
}
