<?php

/**
 * Implements hook_install_tasks()
 */
function opencrm_kickstart_install_tasks() {
  $tasks = array();
  $tasks['opencrm_kickstart_choose_search_provider'] = array(
    'display_name' => st('Select Search Service'),
    'display' => TRUE,
    'type' => 'form',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'opencrm_kickstart_choose_search_provider',
  );

  $tasks['opencrm_kickstart_configure_search_server'] = array(
    'display_name' => st('Configure Search Service'),
    'display' => TRUE,
    'type' => 'form',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'opencrm_kickstart_configure_search_api',
  );

  return $tasks;
}

/**
 * Implements hook_install_tasks_alter()
 */
function opencrm_kickstart_install_tasks_alter(&$tasks, $install_state) {
  // Magically go one level deeper in solving years of dependency problems
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  $tasks['install_load_profile']['function'] = 'panopoly_core_install_load_profile';

  // Since we only offer one language, define a callback to set this
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  $tasks['install_select_locale']['function'] = 'panopoly_core_install_locale_selection';

  $tasks['install_verify_requirements']['function'] = 'opencrm_install_verify_requirements';

  if (!empty($install_state['opencrm_kickstart_skip_search_api'])) {
    unset($tasks['opencrm_kickstart_configure_search_server']);
  }
}

/**
 * Override the requirement verification step.
 *
 * This is a hack to work around the problem that install profiles do not work
 * with modules that have more complicated requirements, e.g. with specific
 * versions.
 */
function opencrm_install_verify_requirements(&$install_state) {
  // We need to filter out any modules with brackets in there name.
  $dependencies = &$install_state['profile_info']['dependencies'];
  foreach ($dependencies as &$module_name) {
    if ($pos = strpos($module_name, ' ')) {
      $module_name = substr($module_name, 0, $pos);
    }
  }

  return install_verify_requirements($install_state);
}

/**
 * Form callback for the search_api service selection form.
 */
function opencrm_kickstart_choose_search_provider($form, &$form_state) {
  drupal_set_title(st('Select Search Service'));

  $form['provider'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => st('Search Service'),
    '#description' => st('Select a Service to use as your Search Provider. You will be asked to configure this service in the next step.'),
  );

  $form['provider']['class'] = array(
    '#type' => 'select',
    '#title' => t('Service class'),
    '#description' => t('Choose a service class to use.'),
    '#options' => array(
      'search_api_db_service' => st('Database service'),
      'search_api_solr_service' => st('SOLR service'),
    ),
    '#empty_value' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
    '#weight' => 15,
  );

  return $form;
}

/**
 * Service Selection form validate callback.
 */
function opencrm_kickstart_choose_search_provider_validate($form, &$form_state) {
  $class = $form_state['values']['provider']['class'];
  if ($class && !in_array($class, array('search_api_db_service', 'search_api_solr_service'))) {
    form_set_error('provider][class', st('Invalid provider selected.'));
  }
}

/**
 * Service Selection form submit callback.
 */
function opencrm_kickstart_choose_search_provider_submit($form, &$form_state) {
  if (!$form_state['values']['provider']['class']) {
    $form_state['build_info']['args'][0]['opencrm_kickstart_skip_search_api'] = TRUE;
    return;
  }

  $provider_map = array(
    'search_api_db_service' => 'search_api_db',
    'search_api_solr_service' => 'search_api_solr',
  );

  $provider = $form_state['values']['provider']['class'];

  // Make sure the module that provides this service is enabled.
  if (!module_exists($provider_map[$provider])) {
    module_enable(array($provider_map[$provider]));
  }

  // Save the provider to a variable.
  variable_set('opencrm_kickstart_search_service', $provider);
}

/**
 * Form callback for the search_api server configure form.
 *
 * @see search_api_admin_add_server
 */
function opencrm_kickstart_configure_search_api($form, &$form_state) {
  form_load_include($form_state, 'inc', 'search_api', 'search_api.admin');

  $form_state['values']['class'] = variable_get('opencrm_kickstart_search_service', 'search_api_solr_service');drupal_set_message($form_state['values']['class']);
  $form = search_api_admin_add_server($form, $form_state);

  // Don't let them edit the search Service.
  $form['class']['#disabled'] = TRUE;

  $form['description']['#default_value'] = st('The default Search Server for Database Searching and Faceted Browsing.');

  $form['#validate'][] = 'search_api_admin_add_server_validate';
  $form['#submit'][] = 'search_api_admin_add_server_submit';
  $form['#submit'][] = 'opencrm_kickstart_configure_search_api_submit';

  drupal_set_title(st('Configure Search Service'));

  return $form;
}

/**
 * Submit the search api server configuration form.
 */
function opencrm_kickstart_configure_search_api_submit($form, &$form_state) {
  $values = $form_state['values'];

  // Save a server
  variable_set('opencrm_search_api_server', $values['machine_name']);

  // Rebuild the default party_index.
  entity_defaults_rebuild(array('search_api_index'));
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function opencrm_kickstart_form_install_configure_form_alter(&$form, $form_state) {
  // Hide some messages from various modules that are just too chatty.
  drupal_get_messages('status');
  drupal_get_messages('warning');

  // Set reasonable defaults for site configuration form
  $form['site_information']['site_name']['#default_value'] = 'opencrm_kickstart';
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['server_settings']['site_default_country']['#default_value'] = 'GB';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/London'; // The UK, just the best.

  // Define a default email address if we can guess a valid one
  if (valid_email_address('admin@' . $_SERVER['HTTP_HOST'])) {
    $form['site_information']['site_mail']['#default_value'] = 'admin@' . $_SERVER['HTTP_HOST'];
    $form['admin_account']['account']['mail']['#default_value'] = 'admin@' . $_SERVER['HTTP_HOST'];
  }
}
