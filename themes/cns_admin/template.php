<?php
/**
 * @file
 */

/**
 * Implements hook_preprocess_html().
 */
function cns_admin_preprocess_html(&$vars) {
  // Add our flex class to body.
  $vars['classes_array'][] = 'flex-container';

  if (cns_admin_title_img()) {
    $vars['classes_array'][] = 'opencrm-title-img';
  }
}

/**
 * Implements hook_preprocess_block().
 */
function cns_admin_preprocess_block(&$vars) {
  // Add our flex classes to system main.
  if ($vars['block']->module == 'system' && $vars['block']->delta == 'main') {
    $vars['classes_array'][] = 'flex-fill';
    $vars['classes_array'][] = 'flex-container';
  }
}

/**
 * Get/set the title image for a page.
 *
 * @param string $img
 *   An image tag.
 * @param bool $replace
 *   If TRUE, $img will replace rather than append to existing.
 *
 * @return string
 *   The image tag or an empty string.
 */
function cns_admin_title_img($img = NULL, $replace = FALSE) {
  $title_img = &drupal_static(__FUNCTION__, '');

  if ($replace) {
    $title_img = '';
  }

  if ($img) {
    $title_img .= filter_xss($img, array('img'));;
  }

  return $title_img;
}

/**
 * Implements hook_preprocess_HOOK() for panels_pane.
 */
function cns_admin_preprocess_panels_pane(&$vars) {
  if (in_array('opencrm-title-img', $vars['classes_array'])) {
    $output = filter_xss(drupal_render($vars['content']), array('img'));
    if ($output) {
      cns_admin_title_img($output);
      $vars['content'] = array();
    }
    else {
      show($vars['content']);
    }
  }
}

function cns_admin_preprocess_page(&$vars) {
  $vars['title_img'] = cns_admin_title_img();
}
