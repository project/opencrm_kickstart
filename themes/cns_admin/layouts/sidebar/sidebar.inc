<?php

$plugin = array(
  'title' => t('Standard CNS Admin (with sidebar)'),
  'category' => t('CNS Admin'),
  'icon' => 'sidebar.png',
  'theme' => 'panels_sidebar',
  'css' => 'sidebar.css',
  'admin css' => 'sidebar-admin.css',
  'regions' => array(
    'sidebar' => t('Sidebar First'),
    'sidebar_second' => t('Sidebar Second'),
    'content' => t('Content'),
  ),
);
