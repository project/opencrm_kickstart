<?php

/**
 * Implements hook_install().
 *
 * Set up a default front page.
 */
function opencrm_kickstart_install() {
  // Clear our caches.
  drupal_flush_all_caches();
  drupal_static_reset('party_get_data_set_info');

  // Tweak the permissions.
  if ($role = user_role_load_by_name('editor')) {
    user_role_grant_permissions($role->rid, array(
      'bypass node access',
      'administer nodes',
      'administer advanced pane settings',
      'view parties',
      'create url aliases',
      'view the administration theme',
      'add media from remote sources',
      'access administration menu',
      'bypass file access',
      'administer file types',
      'administer files',
      'create files',
      'use text format panopoly_wysiwyg_text',
      'use text format panopoly_html_text',
      'administer menu',
      'create panopoly_page content',
      'edit own panopoly_page content',
      'edit any panopoly_page content',
      'change layouts in place editing',
      'create parties',
      'edit parties',
      'archive parties',
      'view party attached profile2_party_org',
      'attach party profile2_party_org',
      'edit party attached profile2_party_org',
      'view party attached profile2_party_indiv',
      'attach party profile2_party_indiv',
      'edit party attached profile2_party_indiv',
      'view party attached profile2_party_notes',
      'attach party profile2_party_notes',
      'edit party attached profile2_party_notes',
      'save draft',
      'switch shortcut sets',
      'send newsletter',
      'access administration pages',
      'edit terms in 1',
      'access user profiles',
      'access contextual links',
    ));
  }

  if ($role = user_role_load_by_name('administrator')) {
    user_role_grant_permissions($role->rid, array_keys(module_invoke_all('permission')));
    // Set this as the administrator role.
    variable_set('user_admin_role', $role->rid);
  }

  if ($role = user_role_load_by_name('authenticated user')) {
    user_role_grant_permissions($role->rid, array(
      'view own party',
      'edit own party',
      'view own party attached profile2_party_indiv',
      'edit own party attached profile2_party_indiv',
      'subscribe to newsletters',
    ));
  }

  // Add the manager role.
  $manager_role = new stdClass();
  $manager_role->name = 'manager';
  $manager_role->weight = 1;
  if (user_role_save($manager_role)) {
    user_role_grant_permissions($manager_role->rid, array(
      'access administration menu',
      'flush caches',
      'administer blocks',
      'bypass email confirmation',
      'administer facets',
      'administer fieldgroups',
      'bypass file access',
      'administer file types',
      'administer files',
      'view private files',
      'use text format panopoly_wysiwyg_text',
      'use text format panopoly_html_text',
      'use text format full_html',
      'administer menu',
      'create mini panels',
      'administer mini panels',
      'bypass node access',
      'administer content types',
      'administer nodes',
      'access content overview',
      'create panopoly_page content',
      'edit own panopoly_page content',
      'edit any panopoly_page content',
      'administer group',
      'use panels in place editing',
      'change layouts in place editing',
      'administer parties',
      'view parties',
      'create parties',
      'edit parties',
      'archive parties',
      'view party attached profile2_party_org',
      'attach party profile2_party_org',
      'edit party attached profile2_party_org',
      'view party attached profile2_party_indiv',
      'attach party profile2_party_indiv',
      'edit party attached profile2_party_indiv',
      'view party attached profile2_party_notes',
      'attach party profile2_party_notes',
      'edit party attached profile2_party_notes',
      'administer url aliases',
      'create url aliases',
      'administer pathauto',
      'administer profile types',
      'save draft',
      'administer shortcuts',
      'administer newsletters',
      'administer simplenews subscriptions',
      'administer simplenews settings',
      'send newsletter',
      'administer site configuration',
      'access site in maintenance mode',
      'view the administration theme',
      'block IP addresses',
      'administer taxonomy',
      'edit terms in 1',
      'delete terms in 1',
      'administer users',
      'access user profiles',
      'administer views',
      'access all views',
      'access contextual links',
    ));
  }

  // Undo some of the rubbish of Panopoly.
  variable_del('panels_page_allowed_layouts');
  variable_del('panels_page_allowed_types');

  // Enable the dblog module.
  // We enable this here rather than in the dependencies so that site admins
  // can choose at a later date to use syslog instead.
  module_enable(array('dblog'));
}

/**
 * Tweak the permissions.
 */
function opencrm_kickstart_update_7101() {
  if ($role = user_role_load_by_name('editor')) {
    user_role_grant_permissions($role->rid, array(
      'use text format full_html',
      'bypass node access',
      'administer nodes',
      'administer advanced pane settings',
      'use ipe with page manager',
      'view parties',
      'create url aliases',
      'access toolbar',
      'view the administration theme',
    ));
  }

  if ($role = user_role_load_by_name('administrator')) {
    user_role_grant_permissions($role->rid, array(
      'administer opencrm_kickstart',
    ));
  }

  if ($role = user_role_load_by_name('authenticated user')) {
    user_role_grant_permissions($role->rid, array(
      'view own party',
      'edit own party',
    ));
  }
}

/**
 * Add manager and update permissions.
 */
function opencrm_kickstart_update_7102() {
  $all_permissions = array_keys(module_invoke_all('permission'));

  // Add the manager role if it doesn't exist.
  if (!user_role_load_by_name('manager')) {
    $manager_role = new stdClass();
    $manager_role->name = 'manager';
    $manager_role->weight = 1;
    if (user_role_save($manager_role)) {
      $permissions = array_intersect(array(
        'access administration menu',
        'flush caches',
        'administer blocks',
        'bypass email confirmation',
        'administer facets',
        'administer fieldgroups',
        'bypass file access',
        'bypass file access',
        'view private files',
        'use text format panopoly_wysiwyg_text',
        'use text format panopoly_html_text',
        'use text format full_html',
        'administer menu',
        'create mini panels',
        'administer mini panels',
        'bypass node access',
        'administer content types',
        'administer nodes',
        'access content overview',
        'create panopoly_page content',
        'edit own panopoly_page content',
        'edit any panopoly_page content',
        'administer group',
        'use panels in place editing',
        'change layouts in place editing',
        'view parties',
        'create parties',
        'edit parties',
        'archive parties',
        'view party attached profile2_party_org',
        'attach party profile2_party_org',
        'edit party attached profile2_party_org',
        'view party attached profile2_party_indiv',
        'attach party profile2_party_indiv',
        'edit party attached profile2_party_indiv',
        'view party attached profile2_party_notes',
        'attach party profile2_party_notes',
        'edit party attached profile2_party_notes',
        'administer opencrm_kickstart',
        'administer url aliases',
        'create url aliases',
        'administer pathauto',
        'administer profile types',
        'save draft',
        'administer shortcuts',
        'administer newsletters',
        'administer simplenews subscriptions',
        'administer simplenews settings',
        'send newsletter',
        'administer site configuration',
        'access site in maintenance mode',
        'view the administration theme',
        'block IP addresses',
        'administer taxonomy',
        'edit terms in 1',
        'delete terms in 1',
        'administer users',
        'access user profiles',
        'administer views',
        'access all views',
      ), $all_permissions);
      user_role_grant_permissions($manager_role->rid, $permissions);

      $permissions = array_intersect(array(
        'search content',
        'administer shortcuts',
        'customize shortcut links',
        'switch shortcut sets',
      ), $all_permissions);
      user_role_revoke_permissions($manager_role->rid, $permissions);
    }
  }

  // Add role to the editor.
  if ($editor_role = user_role_load_by_name('editor')) {
    $permissions = array_intersect(array(
      'access administration menu',
      'bypass file access',
      'create files',
      'use text format panopoly_wysiwyg_text',
      'use text format panopoly_html_text',
      'administer menu',
      'create panopoly_page content',
      'edit own panopoly_page content',
      'edit any panopoly_page content',
      'change layouts in place editing',
      'create parties',
      'edit parties',
      'archive parties',
      'view party attached profile2_party_org',
      'attach party profile2_party_org',
      'edit party attached profile2_party_org',
      'view party attached profile2_party_indiv',
      'attach party profile2_party_indiv',
      'edit party attached profile2_party_indiv',
      'view party attached profile2_party_notes',
      'attach party profile2_party_notes',
      'edit party attached profile2_party_notes',
      'save draft',
      'switch shortcut sets',
      'send newsletter',
      'access administration pages',
      'edit terms in 1',
      'access user profiles',
    ), $all_permissions);
    user_role_grant_permissions($editor_role->rid, $permissions);

    // Remove some permissions we don't want.
    $permissions = array_intersect(array(
      'create fieldable quick_links',
      'edit fieldable quick_links',
      'delete fieldable quick_links',
      'create fieldable basic_file',
      'edit fieldable basic_file',
      'delete fieldable basic_file',
      'create fieldable image',
      'edit fieldable image',
      'delete fieldable image',
      'create fieldable text',
      'edit fieldable text',
      'delete fieldable text',
      'create fieldable map',
      'edit fieldable map',
      'delete fieldable map',
      'create fieldable table',
      'edit fieldable table',
      'delete fieldable table',
      'create fieldable video',
      'edit fieldable video',
      'delete fieldable video',
      'bypass node access',
      'administer content types',
      'administer nodes',
      'delete own panopoly_page content',
      'delete any panopoly_page content',
      'use page manager',
      'use ipe with page manager',
      'search content',
      'administer taxonomy',
    ), $all_permissions);
    user_role_revoke_permissions($editor_role->rid, $permissions);
  }

  if ($role = user_role_load_by_name('authenticated user')) {
    $permissions = array_intersect(array(
      'view own party attached profile2_party_indiv',
      'edit own party attached profile2_party_indiv',
      'subscribe to newsletters',
    ), $all_permissions);
    user_role_grant_permissions($role->rid, $permissions);
  }
}

/**
 * Undo some of the rubbish of Panopoly.
 */
function opencrm_kickstart_update_7103() {
  variable_del('panels_page_allowed_layouts');
  variable_del('panels_page_allowed_types');
}
